---
layout: post
title: 使用 Jekyll 創建靜態網頁
permalink: create-jekyll.html
date: 2021-04-05
author: Ken Chang
categories: [Web, Jekyll]
tags: [Web, Jekyll] 
toc: true
---

部落格從 [Hugo](https://gohugo.io/) 搬到 [Jekyll](https://jekyllrb.com/) 已過了一段時間，會搬移的原因只是比較喜歡 Jekyll 的 [Theme](https://jekyllrb.com/resources/) 風格。我第一個使用的靜態網頁產生框架是 [Hexo](https://hexo.io/zh-tw/)，後來改用 [Hugo](https://gohugo.io/)，最近換到 [Jekyll](https://jekyllrb.com/)，用這三種框架創建靜態網頁的流程非常類似，每種框架有不同的雷，但 Google 都可以找到解法，問題都不大。

## 1. 安裝 Jekyll
從 [Jekyll](https://jekyllrb.com/docs/) 依照官網步驟安裝。由於 Jekyll 是使用 Ruby 寫的，所以會需要安裝 Ruby，而安裝 Ruby 後，需要再安裝其他工具。
* 管理環境語言版本的工具，rbenv 或 RVM
* 管理 Ruby 套件的 Gem
* 管理 Ruby 套件版本的 Bundler

如上述都有完成，就可以透過 `bundle exec jekyll s` 看到網站

## 2. 修改 theme 為 Chirpy
在 [Chirpy](https://github.com/cotes2020/jekyll-theme-chirpy/) 看 README 安裝
1. Gemfile 加上 `gem "jekyll-theme-chirpy"`
2. 修改 _config.yml 裡的 theme 為 `theme: jekyll-theme-chirpy`

   其實上面兩個步驟是多做，因為接下來他會請你安裝跟他相同的套件，且設定也需要從設定檔取得，所以直接把他的內容貼到自己的比較快。

3. 把 chirpy 的 Gemfile 內有用到的套件都貼到你的 Gemfile
4. 把 chirpy 的 _config.yml 內容，貼到 你的 _config.yml

透過 `bundle` 安裝套件

如上述都有完成，就可以透過 `bundle exec jekyll s` 看到已有安裝模板的網站

## 3. 修改 ymal 資料
在 _config.yml 修改個人資料、網站資料、網站樣式、追蹤碼...，依自己的需求修改即可。通常有開放的功能選項都會寫出來，照著格式填寫或修改即可。

## 4. 安裝套件 jekyll-gist
由於我個人部落格文章內有許多程式碼用 [Gist](https://gist.github.com) 呈現，故需要安裝 [jekyll-gist](https://github.com/jekyll/jekyll-gist) 套件，否則 Jekyll 無法知道文章內 Gist 的部分該怎麼解析。

## 5. 產生的網站
執行過 `bundle exec jekyll s`，產生的網站會放在 _site 資料夾，只要把內容放到支援靜態網頁的空間即可。故如果是使用 git，可以只 push 這一個資料夾。
