---
title: "使用 iOS 內建 decode 遇到的問題 ( RFC3986 )"
author: "Ken Chang"
tags: ["iOS", "MacOS", "Swift", "Objective-C"]
date: 2020-06-01T00:00:00+08:00
comments: true
draft: false
---

使用 iOS 內建 decode 遇到的問題 ( RFC3986 ) 。最近從一些關鍵字組合網址時，發現 iOS 內建的 decode 並沒有將 & 和 = 符號加在字集裡面。所以導致有些關鍵字在組合時產生錯誤。再仔細用 [RFC3986](https://tools.ietf.org/html/rfc3986) 的規範去測了一下，發現其實滿多的字符內建的 decode 都沒有。

<!--more-->

我們用三個關鍵字來舉例，第三個關鍵字為 RFC3986 的集合：

經過使用 urlQueryAllowed 字集的編碼變成如下表

| 關鍵字|經過urlQueryAllowed字集編碼|
| :--- | :---: |
| H&M碎花裙|H&M%E7%A2%8E%E8%8A%B1%E8%A3%99|
| M&M's牛奶巧克力|M&M's%E7%89%9B%E5%A5%B6%E5%B7%A7%E5%85%8B%E5%8A%9B |
| ~\t \"#%&()+,/:;<=>?@[]\\]\|~ | ~%09%20%22%23%25&()+,/:;%3C=%3E?@%5B%5D%5C%5D%7C~|

我們會發現有一些符號並沒有被編碼。這些沒有被編碼的字符，在某些情況下會影響結構。例如範例裡的&amp;字符，如果有人直接使用含有此字符的關鍵字組合到網址，就會影響到網址的結構。我們可以看看內建的字集有包含哪些字符，在字符以外的，即不會被編碼。

| 關鍵字|經過urlQueryAllowed字集編碼|
| :--- | :---: |
| urlHostAllowed|"#%/<>?@\^`{|} |
| urlFragmentAllowed|"#%<>[\]^`{|} |
| urlPasswordAllowed | "#%/:<>?@[\]^`{|} |
| urlPathAllowed | "#%;<>?[\]^`{|} |
| urlQueryAllowed | "#%<>[\]^`{|}|
| urlUserAllowed | "#%/:<>?@[\]^`|

知道原因以後，解決的方式就很簡單了，我們可以自訂字集，使用自訂的字集編碼。
我的作法是做一個 string 的 category，把 RFC3986 的字集都加進去，之後使用這個字集作編碼。
其中需要注意的是，空白會被轉成%20，而實際上是需要轉成+號。

{{< gist KennC f02720e9ebc472f951b035205250c82e >}}

---

### 測試
{{< gist KennC 7c2ec0e04a6afa67b53adc20df8550c0 >}}
最後會印出%7E%09+%22%23%25%26%28%29%2B%2C%2F%3A%3B%3C%3D%3E%3F%40%5B%5D%5C%5D%7C%7E，是我要的結果。

如果有寫 test case 的話，也可以參考。有一支EncodeUtils，做 encode 的部分都在 EncodeUtils的EncodeUseRFC3986WithString 這個方法裡做。
{{< gist KennC ede013ba702d8129bd323f574c223723 >}}

