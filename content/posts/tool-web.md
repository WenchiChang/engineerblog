---
title: "幾個好用的網站"
author: "Ken Chang"
tags: ["Tools"]
date: 2020-06-01T00:00:00+08:00
comments: true
draft: false
toc: true
---

幾個好用的網站，有圖庫、照片、縮圖、UI設計、顏色色碼、字典、設計icon、英文字典、工具。

<!--more-->

---

##  **圖庫**

有時需要icon圖，可以從下列這幾個網站抓，抓圖時需要注意一下使用協議。尤其有要商用的話就更需要小心。

iconfinder <https://www.iconfinder.com>


easyicon <http://www.easyicon.net>


findicons <http://findicons.com>


Icon Ninja <http://www.iconninja.com>


illustAC <https://en.ac-illust.com>

---
## **照片**
Unsplash <https://unsplash.com/images/stock/public-domain>

目前 banner 照片源自該網站的 Timothy Meinberg - Cat in a bay

---
## **縮圖**

TinyPNG <https://tinypng.com>

makeappicon <https://makeappicon.com>

---

## **UI設計**

cocoacontrols <https://www.cocoacontrols.com>

---

## **顏色色碼**
UIColor <http://uicolor.xyz/#/hex-to-ui>

---

## **設計icon**

iconsflow <https://iconsflow.com>

---
## **英文字典**

Cambridge Dictionary <https://dictionary.cambridge.org>

---

## **工具**

quicktype https://quicktype.io
