---
title: "簡單封裝 AFNetworking 3.0"
author: "Ken Chang"
tags: ["iOS", "MacOS", "Swift", "Objective-C"]
date: 2020-06-01T00:00:00+08:00
comments: true
draft: true
---

在 AFNetworking3.0 安裝好以後，有些人會直接使用在程式碼裡面。但較好的做法是我們封裝 AFNetworking ，讓我們之後要更改關於網路連線的部分統一修改一個地方就好，不需要在每個做請求的部份都要在設定，也簡少程式碼，分工更明確，這邊只做簡單的封裝。其實所有使用的第三方套件，都要自己封裝過再用，未來才較好維護。

實作了兩個 block，一個是回傳失敗放入 NSError，一個是成功回傳放入 dictionary。做了 typedef enum，來決定 HTTP Method。以及 singleton 要用的共用接口，和一個 function。

<!--more-->

{{< gist KennC 67e48e6bf3e2f1a59b2f04981ef0f659 >}}

接著實作singletion，以及在初始化req的設定。實做requestWithMethod。

{{< gist KennC c39de214938e13a9028ea2fade5e7673 >}}

在ViewController裡就可以簡短的使用。

{{< gist KennC 8116f3790cc0cd8d573b4f7284c66859 >}}

簡單封裝好的網路層就完成了。如果還要更進階，可以再繼續加入 auth、error handling、exception handling...各種處理。有些工程師甚至會把 path 也包在下一層，只露出 params 部分。
