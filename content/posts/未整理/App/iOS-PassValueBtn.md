---
title: "Pass value when Button clicked"
author: "Ken Chang"
tags: ["iOS", "MacOS", "Swift", "Objective-C"]
date: 2020-06-01T00:00:00+08:00
comments: true
draft: true
---

有時需要在點擊某個按鈕時，把資料跟著一起帶到點擊事件裡。有些人會把資料藏在沒用到的屬性，例如：Tag。較正確的解法其實還是用 Delegate ，但如果要自己客製化一個帶資料的按鈕。也就是繼承一個 UIButton，自己增加一個 value，也可以做到一樣的效果。通常會走到這一步，前面的流程或程式設計可能要再想清楚，也許有可以設計得更好的地方。因為資料歸資料，按鈕歸按鈕，做一個帶資料的按鈕，其實在設計上是不合邏輯的。

<!--more-->

{{< gist KennC c3feb93e6497d9c728f151738c86129b >}}

{{< gist KennC 987912557c7a6991a794e3dadfe1e0d7 >}}

運行後點下 btnPassValue 就可以在 click 裡收到剛才的塞在裡面的 passString 資料。
