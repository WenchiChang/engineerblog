---
title: "Loading and Caching Images in SwiftUI"
author: "Ken Chang"
tags: ["iOS", "MacOS", "Swift", "SwiftUI", "Objective-C"]
date: 2020-06-09T14:28:20+08:00
comments: true
draft: true
---
iOS App 開發時，在圖片下載的部分，許多人會直接使用 library。快速做好圖片下載、快取。Objective-C 大部分使用的是 [SDWebImage](https://github.com/SDWebImage/SDWebImage)。Swift 大部分使用的是 [Kingfisher](https://github.com/onevcat/Kingfisher)。可設定參數功能都很多，但是大部分的 App 都只有用來下載、快取圖片。
<!--more-->
我想目前沒有幾個工程師有接到需求像是「當圖片快取超過 20M 就刪除快取」這一類的吧？所以如果沒有特別注意他們的多重快取，App 會隨著使用時間越久，佔的容量越大。這是因為多重快取有存在 disk 的緣故。最近開始使用 SwiftUI，上網找了資料，實現一個下載、快取圖片的機制，目前只有快取在記憶體。

使用上只要`UrlImageView(urlString: animal.albumFile)`即可

{{< gist KennC a5b9784cb9f10b2b726c955e374dd2cb >}}

{{< gist KennC f6325ec5e6d0a92f387cf0fbf68c3e49 >}}

參考資料：
https://www.youtube.com/watch?v=volfJt7mupo
