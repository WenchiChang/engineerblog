---
title: "使用 Objective-C 實作 KVO (Observer Pattern應用)"
author: "Ken Chang"
tags: ["iOS", "MacOS", "Swift", "Objective-C", "Design Patterns"]
date: 2020-06-01T00:00:00+08:00
comments: true
draft: true
---

這篇文章主要是 使用 Objective-C 實作 KVO (Observer Pattern)。KVO(Key-Value-Observing)觀察數值。當看到 Observer Pattern，add、observe、remove 就是這三個方法要出現了！

以下就是 Objective-C 對應的三種函數：

增加監聽```addObserver: forKeyPath: options:context:```

接收監聽```observeValueForKeyPath:ofObject:change:context:```

移除監聽```removeObserver:observer forKeyPath:```

這篇範例會開一個 PersonModel class，對他做 singleton 後。觀察裡面的 address 跟 name 兩個數值。並實作方法在 PersonObserver class。

<!--more-->

## PersonModel
{{< gist KennC 4a053e164163d7ee91aa5e58e20b6c9b >}}
{{< gist KennC edce6bf364c03fbbee13b951ad8a4251 >}}

------
實作觀察者在觀察到以後要做什麼事的 class 跟 method
## PersonObserver.m

{{< gist KennC 33acb8f4cdab4f298486ce8246481f9c >}}

至此你已經有準備要被觀察的對象(PersonModel 裡 address 跟 name)

以及觀察者的實作(PersonObserver 裡 addObserver: forKeyPath: options:context:)

接著就可以讓觀察者，觀察要被觀察的對象

{{< gist KennC a1472bb19aa0757b4301c96b5e352b0a >}}

只要當數值被改變了，實作在 PersonObserver 的 observeValueForKeyPath:ofObject:change:context: 就會收到數值被修改的通知。

以範例來看，流程如下:
觀察 aPerson 裡的 name，改變數值為 LiLei，此時進入實作方法。
觀察 aPerson 裡的 address，改變數值為 street，此時進入實作方法。
最後移除兩個觀察者。

以上的 Objective-C 範例原始碼，放在 [Github連結](https://github.com/KennC/ObjC_KVO) 提供參考。
