---
title: "SDWebImage 清除 cache"
author: "Ken Chang"
tags: ["iOS", "MacOS", "Swift", "Objective-C"]
date: 2020-06-01T00:00:00+08:00
comments: true
draft: true
---

近期接個要做第二階段的 App，安裝到手機裡使用一段時間後，App 容量會越來越大。這種通常都是 local cache，但是用了好久以後發現似乎沒有清除 cache，所以就到code 裡搜尋。果然程式裡沒有清除圖片 cache。而這個 App 正好需要從網路抓很多圖片顯示給使用者...。

其實在 iOS 開發，大家用的 lib 就是那幾套，這App是使用 SDWebImage 抓圖片。SDWebImage 的清除機制很簡單，有依照大小，依照時間，依照圖的size，自己選擇條件做清理。

如果真的不曉得怎麼算，或是業主也沒有一個準則，也可直接設定當 App 的 cache 超過20M就全清，或是你想 cache 更多，這樣做是最簡單的。直接在 didFinishLaunching 裡 [[SDImageCache sharedImageCache] setMaxCacheSize:20 * 1024 * 1024]; 即可，每次開啟 App 就會去檢查，超過 20M 就清理。

<!--more-->

------

##### **cache超過20M就刪除cache**

[[SDImageCache sharedImageCache] setMaxCacheSize:20 * 1024 * 1024];

------

##### **100x100像素的圖 cache 10張**

[[SDImageCache sharedImageCache] setMaxMemoryCost:10 * 100 * 100];

------

##### **清除過期圖片(要設置期限)**

[[SDImageCache sharedImageCache] cleanDisk];

------

##### **期限設為7天**

[[SDImageCache sharedImageCache] setMaxCacheAge:7 * 24 * 60 * 60];

------

##### **大圖不解壓縮(消耗大量內存)**

[[SDImageCache sharedImageCache] setShouldDecompressImages:NO];

[[SDWebImageDownloader sharedDownloader] setShouldDecompressImages:NO];
