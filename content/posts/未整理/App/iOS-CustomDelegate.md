---
title: "自己做 delegate & protocol 機制"
author: "Ken Chang"
tags: ["iOS", "MacOS", "Swift", "Objective-C"]
date: 2020-06-02T00:00:00+08:00
comments: true
draft: true
---

有時要把一些動作實作在另一個類別上，這時就需要自己寫 delegate。例如：今天我們有個畫面，畫面裡有個搜尋列表，搜尋列表上方有一個 filterView，而 filterView 要做成共用類別，當我們在共用類別上點擊的時候，需要觸發自己寫的 delegete，並實作在另一個類別上。
程式碼的部分很少，重要的是這種 delegate 的概念，幾乎佔 iOS 開發的七～八成。

<!--more-->

步驟大致如下

- Step.1 寫好 protocol
- Step.2 寫好 property
- Step.3 告訴畫面要實作這個 delegate、並真的在畫面裡實作 delegate。
- Step.4 使用你的 delegate ，把資料傳出去。

範例：做一個可以過濾性別的小功能。我們做一個 FilterView，裡面有兩個按鈕，可以選擇性別，按下後會觸發我們自己做的 delegate，我們的 protocol 以及 property 就是要寫在這個 FilterView 裡面。之後在畫告訴畫面要使用這個 delegate，且在這個畫面裡實作這些 delegate。而我在實作的裡在做過濾資料以及刷新畫面。

---

* #### 在 filterView 裡面需要做 Step.1、Step.2、Step.4
    {{< gist KennC 21fcd5a4f42361b171e13cc1213946e8 >}}
* #### Step.3 告訴畫面要實作這個 delegate、並真的在畫面裡實作 delegate。
    告訴viewcontroller要使用這個delegate，且在這個viewcontroller裡implement這些delegate。
    {{< gist KennC 3df89609de13ce7ad4697c533565cbda >}}
    記得把filterDelegate設給self
    {{< gist KennC ae8216bc43a38e587d91525089ce0b6b >}}
    implement這兩個delegate
    {{< gist KennC cc4f296d44b5ecf596c1a9468bf7011e >}}
