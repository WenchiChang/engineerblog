---
title: "使用 fastlane snapshot 自動截圖"
author: "Ken Chang"
tags: ["iOS", "MacOS", "Swift", "Objective-C", "Design Patterns", "CI/CD"]
date: 2020-06-01T00:00:00+08:00
comments: true
draft: true
---

一般來說這個截圖是為了要上傳到 itune connect ，當 App 上架到 Apple Store 後 。使用者可以在 Apple Store 先預覽這些圖當做參考。通常是由設計師去負責，因為有時候設計師會想要一些美化，但沒有設計師時，工程師要自己截圖，礙於需要多種尺寸，截圖需要花很多時間，使用 snapshot 就可以省下這些時間。 Snapshot 是基於 UI Test 做的，我們需要在 UI Test 寫一些程式碼，接著透過 snapshot 的一些函式，就可以幫我們截圖。當然如果要自己手動從模擬器截圖也是可以的。

<!--more-->

首先安裝fastlane，網路上已經有很多教學，不過 [fastlane官網安裝流程](https://docs.fastlane.tools/getting-started/ios/setup/) 已經很詳細，照著做沒有遇到太大問題。

安裝好以後，到 Project 的所在位置，初始化 fastlane 後，選擇你要使用拍照功能。就會看到你的專案多了一些檔案。

![](https://images2.imgbox.com/36/2e/BdyfBmMr_o.png)

其實拍照功能只有用到 SnapshotHelper.swift 跟 Snapfile。

接著我們把 SnapshotHelper.swift 拉進專案裡，注意 Targets 要勾選 UITest 的部分。

![](https://images2.imgbox.com/34/0e/m8Iidd4K_o.png)

接著就要來寫 UITest。注意你要點擊的元件都要設定 accessibilityIdentifier 。

{{< gist KennC 8387a13ad70daa2cd9de235d3e44eaa8 >}}

那麼關於設定 Snapfile 的部分，官方也給了詳盡的說明，[Snapfile官方說明](https://docs.fastlane.tools/getting-started/ios/screenshots/)。

填上專案裡對應的參數或是名字，接著下指令 `fastlane snapshot`。

就可以放著讓他執行了，最後執行完，還會用 html 呈現給你看。

這裡我就使用 Side project 做個結果範例。

![](https://images2.imgbox.com/68/59/jKe0oqSP_o.png)

這樣你就自動截圖成功了！
