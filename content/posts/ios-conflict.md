---
title: "iOS 在 merge 後遇到 conflict 的解決順序"
author: "Ken Chang"
tags: ["iOS", "MacOS", "Swift", "Objective-C", "Git"]
date: 2020-06-01T00:00:00+08:00
comments: true
draft: false
toc: true
---

我們假設情況把一個名為 feature_login 的 branch，merge 到 develop 時遇到 conflict。遇到 conflict 時會在程式碼裡發現被加上這些符號
    {{< gist KennC 454ffc23c61b69968016bce23ecefad4 >}}
<!--more-->
------

## Step1.修復.pbxproj的conflict

當 pbxproj 遇到 conflict 時，整個 project 就打不開了。需要先去解決 pbxproj 的 conflict。先讓整個專案可以打開，以利之後修復其他 conflict 的地方。要是其他地方都修復，這個檔案沒有修復，那專案一樣無法開啟。這情況較常發生在專案開發初期，大幅度修改與新增刪除檔案較為頻繁時，容易遇到。

------

## Step2.修復Podfile.lock的conflict

有用到 cocoapod 的專案可能會遇到 Podfile.lock 的 conflict，這時候不要一股腦地就下去修。利用這個檔案的特性：在 pod install 時，會產生一個新的。所以這時應是去檢查 Podfile，如果 Podfile 有 conflict，就把他修好。確認沒有問題以後，直接把 Podfile.lock 刪除，再 pod install。讓他依照好的 Podfile，產生好的 Podfile.lock。

------

## Step3.修復在程式碼裡的conflict

單純在.m與.h的程式碼裡面 conflict，這就容易改了。只要移除你不需要的程式碼，留下需要的程式碼就可以了。也常遇到兩個版本的 code 都要留下來的時候。在專案開發到後期，因為大幅度修改與新增刪除檔案這些行為比起初期較少，所以大部分都會是這狀況。

------

## 如何修復？

直接刪掉不需要的程式碼，包含那些『\<\<\<\<\<\<\<』、『=======』、『\>\>\>\>\>\>\>』，就這麼簡單。

如果想留下在develop branch的部分，就留下以下部分，其他程式碼與符號刪掉。
    {{< gist KennC 96a4afd761c866085cf6e9805a3ce87f >}}

如果想留下在feature_login branch的部分，就留下以下部分，其他程式碼與符號刪掉。
    {{< gist KennC 51554800fe7cda0c4fc7e40f08e7e50b >}}

如果想留下全部的code，就留下以下部分，其他符號都刪掉。
    {{< gist KennC affb95389f70b92a6e7361a0fa923949 >}}
