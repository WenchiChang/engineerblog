---
layout: post
title: iTerm + Oh My Zsh 設定
permalink: /mac-command-line.html
date: 2021-01-02
author: Ken Chang
categories: [Tool]
tags: [Tool] 
toc: true
---

iTerm + Oh My Zsh 設定。由於每次重灌 MAC 後就要上網搜尋一下模板跟字體的載點，以及環境的設定。所以這篇目的是記下步驟、連結資源，未來好找。

<!-- more -->

---

## 安裝

* 安裝 [iTerm2](https://www.iterm2.com)

* 安裝 [oh my zsh](http://ohmyz.sh)

如無法安裝，請確認是否已事先安裝好 Command Line Tools 或是 [Homebrew](https://brew.sh/index_zh-tw.html)。

在 iTerm2 上看到這畫面就是成功安裝 oh my zsh
![](https://images2.imgbox.com/9f/49/TM88b64K_o.png)

---

## 修改theme

從第一次使用以來看網路教學就是都用 agnoster，所以也用得很習慣，有換過別的 theme，不過最後還是換回 agnoster 了。

1. 用 vi 打開 `~/.zshrc` 修改 ZSH_THEME 為 agnoster
2. 重新開啟 iTerm ，就可以看到 theme，字體還是亂碼，改好字體後就不會了。

---

## 調整背景顏色

1. 到 http://ethanschoonover.com/solarized 下載檔案
2. 解壓縮之後打開 iterm2-colors-solarized 資料夾，打開 Solarized Dark.itermcolors 檔案。
3. 在到 iTerm 的 Preferences，選擇 Profiles -> Colors -> Load Presets，選擇 Solarized Dark。

背景這樣就設定好了!

---

## 更換字體

1. 到 https://github.com/supermarin/powerline-fonts 下載，
2. 打開資料夾選擇要用的字體，點兩下之後，記得點擊「安裝字體」。
3. 最後到 iTerm 的 Preferences裡面 Profiles -> Text ，裡面 Change Font 改成你剛剛安裝的字體

這樣就完成了！
