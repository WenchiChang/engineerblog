---
title: "個人 Git 常用指令"
date: 2023-03-11T18:02:49+08:00
draft: false
toc: true
---

記錄我常用的Git指令，有想到會陸續增加。

<!--more-->

## checkout
- `$ git checkout <branch>` 切換分支
- `$ git checkout -b <branch>` 建立新分支，同時切換到新分支

---
## add
- `$ git add .` 所有異動檔案加到暫存區（如果是 Git 1.x 版不會處理「刪除檔案」的異動）
- `$ git add <filename>` 加入此檔案到暫存區。

> 加入檔案到暫存區這個步驟，在我使用 GUI 工具後，改成會在 GUI 工具再看一次自己修改的檔案內容，確認修改部分沒有錯誤後就勾選檔案，工具會把勾選的檔案加到暫存區。

---
## commit
- `$ git commit -m "title" -m "desc"` 將暫存區的檔案儲存，並加入標題跟描述。

> 也可以只加標題或是不特別指定為兩者皆不加，這訊息對接下來的任何人都很重要，最好習慣性都加入。

- `$ git commit --amend` 更改剛才 commit，但尚未 push 的 message

- `$ git commit --allow-empty -m "desc"` commit 一個空的
---
## tag

- `$ git tag` 列出所有標籤
- `$ git tag -a <tagname> -m "desc"` 新增一個 tag，並加入描述。

---

## merge
- `$ git merge --no-ff <branch name>` merge 時關掉 fast-forward

---
## push
 
- `$ git push origin <tagname>` 推一個標籤到遠端
- `$ git push origin --tags` 一次推所有標籤到遠端
- `$ git push -u origin <branch name>` 把 branch push 上去
- `$ git push origin :<branch name>` 刪除遠端的 branch (其實是推一個空的蓋掉遠端的)

---
## cherry-pick
##### 從 branch A 挑出某幾包 commit，至另一個 branch B
- `$ tig` 先在 branch A 看 commit id，copy 下來。
- `$ git cherry-pick <commit id>` 至 branch B 使用 cherry-pick 就可以了
  
> 這時候就是看平常有沒有良好的 commit 習慣，如果常常 commit 不能建置的進度，可能使用這個指令後會衍生其他問題。

---
## Rename branch 
- 流程上是先幫 local 的改名，把 remote 的砍掉，再把本機的 push 上去。
- 進入要更名的 branch。
1. `$ git branch -m <oldBranchName> <newBranchName>`
2. `$ git branch -r` 檢查遠端，確認遠端舊的 branch 還在
3. `$ git push origin :<oldBranchName>` 移除遠端的 branch (其實是推一個空的去把遠端的蓋掉)
4. `$ git push origin <newBranchName>` 把新的 local branch push 上去

> 第四步驟的指令效果等同於`$ git push origin <newBranchName>:<newBranchName>`。用冒號分辨，冒號左邊的是本地端，推到冒號右邊的遠端。 