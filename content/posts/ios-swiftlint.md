---
title: "使用 SwiftLint 管理 Swift 的 coding style"
author: "Ken Chang"
tags: ["iOS", "MacOS", "Swift", "Objective-C"]
date: 2020-06-02T00:00:00+08:00
comments: true
draft: false
toc: true

---

Swift 不像 Python 有 PEP8 規範，或是 Go 可使用 gofmt，這種統一 coding style 的規範或是工具。
開發者的 coding style 常常都不大相同，即使是同一個人在不一樣的地方宣告一樣的東西，也有可能會不一樣。如果是單獨開發專案，作者思考一下還可以喚起回憶。如果是多人開發，裡面會混雜不同的 style，專案時間長，維護的工程師經過輪替，一份專案的 style 通常已經很混亂。為了避免這種狀況，我們可以透過工具來管理 coding stytle。但這只 For 這個專案，也不是統一的標準。

[SwiftLint](https://github.com/realm/SwiftLint) 是由 [Realm Inc](https://realm.io/) 做的。Base on [GitHub's Swift Style Guide](https://github.com/github/swift-style-guide)，再加上 [75 種 rule](https://github.com/realm/SwiftLint/tree/master/Source/SwiftLintFramework/Rules) 做成。關閉/啟動/修改規則，也可以自己透過 `.swiftlint.yml` 來實作。接下來就開始使用 cocoaPod 安裝，並修復幾個違反規範的違規。

<!--more-->

---

## Step 1. 使用 cocoaPod 安裝

`pod 'SwiftLint'`

## Step 2. 加上 Script

找到專案 Run Script 加上 `${PODS_ROOT}/SwiftLint/swiftlint`

## Step 3. 按下 Build 出現大量 warning 和 error

(如果安裝的部分還不行請直接看 [SwiftLint](https://github.com/realm/SwiftLint))
    
出現 822 個 warning 和 116 個 error
    ![](https://images2.imgbox.com/35/22/K6HpY5Va_o.png)
第一次安裝會這樣是很正常的，手邊有幾個案子試著裝起來，都是 warning 和 error 都是 999 以上。
其中有許多是重複的，分享圖中幾個重複較多的解決辦法。

* Lines with only whitespace throw the trailing whitespace warning. => 不能以空白結尾
* Vertical Whitespace Violation: Limit vertical whitespace to a single empty line.currently 2. => 只能空一行
* Line Length Violation: Line should be 120 characters or less: currently 136 characters (line_length) => 一行只能有120字元，此行有136個字元
        
## Step 4. 解決第一個與第二個 warning 和 error（透過設定與自動校正）
前兩個 warning 在做兩個步驟以後，就會幫你自動修正了
步驟一是 Preferences > Text Editing > 打勾 Including whitespace-only lines
    ![](https://images2.imgbox.com/45/18/7jsmmOrK_o.png)
步驟二是在目錄下指令 swiftlint autocorrect (如果 swiftlint not found ，檢查是否全域安裝成功，失敗的話再用 brew install swiftlint 安裝即可。

輸入 `swiftlint autocorrect` 之後就自動幫你解決了許多他可以解決的 error 與 warning。

## Step 5. 解決第三個 warning（關閉與忽略檢查）

要解決第三個 warning ，需要撰寫我們的 `.swiftlint.yml` 的檔案。這個檔案要自己產生在專案的資料夾下(注意: .開頭是隱藏檔，所以直接進去 folder 是看不到的)。如何撰寫就要看文件了。文件可以在 [swiftlint 的 gitHub](https://github.com/realm/SwiftLint) 看一下 Configuration。下方也初步的寫一下 `.swiftlint.yml` 可以消除一些不必要的 warning。

這個字元太長的警告其實可以直接無視，畢竟官方 delegate 就已經很長了，所以可以直接忽略無仿。那要怎麼樣消除這惱人的警告呢?警告訊息後面有說這個是 line_length 的規則，我把這個規則關閉。

我也順手加上把 CocoaPod 忽略檢查，因為我們在使用第三方套件時，都不希望會去更改到套件裡的東西，所以我們不對第三方套件做檢查。(如果有使用 Carthage ，也要把 Carthage 加入)。你的 `.swiftlint.yml` 目前會變成這樣。
    <img class="alignnone size-medium" src="https://images2.imgbox.com/e1/66/0ew2hoLs_o.png" width="660">
<!--    ![](https://images2.imgbox.com/e1/66/0ew2hoLs_o.png) -->

成功以後，再 Build 一次。原 822 個 warning 和 116 個 error，現已變成 45 個 warning 和 33 個 error。
    <img class="alignnone size-medium" src="https://images2.imgbox.com/f7/7d/z2agpMma_o.png" width="660">
<!--   ![](https://images2.imgbox.com/f7/7d/z2agpMma_o.png) -->

---

## 結論
大部分的設定都已在 Configuration 裡，我們只要負責在 `.swiftlint.yml` 裡開關即可。有些只是設定 IDE 認知。以此類推，去檢查每一個 error 與 warning，發現背後原因，決定修改規則或是修改程式。最後 `.swiftlint.yml` 就會是你的 Swift 專案的 coding Style。
