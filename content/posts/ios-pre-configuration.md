---
title: "開發 iOS App 產品時，儘早加入的功能"
date: 2023-03-12T13:04:44+08:00
draft: false
toc: true
---

分享我在做了數個 iOS App 產品後，幾個越早做越好的功能，最好在產品從零創建時，就做進去。如沒有做也是能完成需求，可未來再補，只是到時候可能事倍功半，或是版本差異較大。

<!--more-->
  
---

## 版本控制

版本控制是一定要做的，從我開發軟體以來都是使用 Git。基礎的功能會以後，學會操作分支，就可以學習 Flow 套到開發流程裡面。
推薦的介紹、教學：
- [連猴子都能懂的Git入門指南](https://backlog.com/git-tutorial/tw/)
- [為你自己學Git](https://gitbook.tw/)
- [Git Flow](https://nvie.com/posts/a-successful-git-branching-model/)
- [Github Flow](https://guides.github.com/introduction/flow/)

---

## 環境區分
不論是使用 Targets 或是 Configuration 做環境區分都可以。這也會影響到使用其他服務時，需使用不同環境的設定檔。而環境也是需要維護的，故視需求增加就好。但至少要有兩個以上，develop 跟 production，視公司情況也許還會需要 staging、testing。

---

## 更新機制
軟體剛上線時，新增的功能很多，使用者需要更新，才能擁有新功能，這時候會需要推薦使用者更新。如果沒有推薦機制，除非使用者裝置自動更新，或是重新安裝，或是使用者手動到商店更新，才會有新功能。或是目前使用者的軟體版本有重大 Bug，需要更新才能解決，也是需要此機制。更新機制可以做成要強制更新或是讓使用者選擇是否要更新。即使沒有後端支援，也可以透過 Apple 的 lookup 做。

---

## 語意化版本號
創建 iOS App 專案時，預設 Version 只有兩碼，但我會較建議[語意化版本號](https://semver.org/lang/zh-TW/)。

---

## Crash Analysis
紀錄 App Crash 的狀況，提供工程師查閃退的原因。目前較普遍的是使用 [Firebase Crashlytics](https://firebase.google.com/docs/crashlytics)。
    
---

## 數據分析工具
紀錄使用者行為，提供數據給其他部門參考的工具，此類型的工具有很多，目前我是使用 [Firebase Analytics](https://firebase.google.com/docs/analytics) 連接到 [Google Analytics](https://marketingplatform.google.com/about/analytics/)。如果只是想看簡單的點擊數據，可以不用連接，官方文件有指出核心是一樣的，未來有需要再開連接也可以。

---

## 字串配置
主要是未來如果有需求是使用者可以從 App 內部換語系，到時候需要找出每個檔案的每個文案統整到 string 檔案。每個 Storyboard 上有文字的元件，都需要拉出 IBOutlet，非常耗時耗力。所以如果前置有做到這項，平常就是這樣處理文案，就不用到時候需要花大量工時。Android 專案開始就有此配置，且使用 Key 抓取文案時有自動完成。iOS 這部分就只能工程師自己手動建置。

---

## Launch app procedure
每個 App 啟動的時候，從使用者啟動 App，直到 App 顯示第一個畫面，其實過程都會觸發許多檢驗、取資料。有些必須要過的檢驗，或是必須要取得的資料，需要有程序管理。如果這程序沒有通過，就會依照狀況做處理。例如：
- Token expired，可嘗試幫他更新 token。
- Server Error，將使用者擋下，並顯示提示。

---

## 各種 Interface、字型、顏色 配置

各種共用的 UI 最好有統一對外介面，既使實作是先用內建的也沒有關係。例如：Alert、Loading、Toast...。字型可以透過 [Appearance Customization](https://developer.apple.com/documentation/uikit/appearance_customization) 將所有字型修改成頻方體。顏色在近幾個 Xcode 版本都有新增不同的選擇方式，例如：Color Literal、Accent Color，喜歡用程式寫也可以，無論如何就是需要整理，不要四處散在各地。

---
