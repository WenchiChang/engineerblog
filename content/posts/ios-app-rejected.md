---
title: "iOS Mobile App Rejected"
date: 2023-03-11T17:00:57+08:00
draft: false
toc: true
---

## 前言
分享我遇過的駁回原因，列出信件內容，並說明信件、導致原因以及我的解決方式，供遇到相同問題的開發者參考。每個審核人員看的角度都不太一樣，所以解決方式不一定適用。

如果你對被駁回的原因有異議，可以回信給 Apple 申訴，有時是審核人員使用方式錯誤，或是認知問題，都可以得到更詳細的解釋。

或是甚至 Apple Guideline 沒有詳述，而審核人員卻抓很嚴，也可以提出覆議，不過這一項通常無效，畢竟解釋方式是以審核人為主。現在 iOS 有內建螢幕錄影，必要時還是需要螢幕錄影加講解。


---

## 駁回原因


* ### Invalid sdk value
> Dear developer,
> 
> We have discovered one or more issues with your recent submission for “Your app name”. To process your submission, the following issues must be corrected:
> 
> Invalid sdk value - The value provided for the sdk portion of LC_VERSION_MIN_IPHONEOS in name.app/name is 10.2 which is greater than the maximum allowed value of 10.1.
> 
> Once these issues have been corrected, use Xcode or Application Loader to upload a new binary to iTunes Connect. Choose the new binary on the app’s Details page in My Apps on iTunes Connect, and click Submit for Review.Regards,
> 
> The App Store team

* #### 信件大綱
  大致上是說你使用的SDK版本(10.2)比 Apple 後台版本(10.1)還要新。

* #### 導致原因
  如大綱
  1. 使用 beta 版的 Xcode上傳你開發的 App 到 iTuneConnect，就會收到這信件。beta 版的 Xcode，裡面有許多 sdk 也是 beta 版，比 iTuneConnect 後端的版本還要新。
  2. 你的 Xcode 更新好了，而 iTuneConnect 後端還沒更新好，這一般只會發生在 Xcode 剛釋出新版本的時候。

* #### 解決方式
  使用舊版的 Xcode 上傳，你要再去載上一版本的 Xcode。(求助未更新Xcode的同事電腦較快)
  等待 iTuneConnect 更新，一般會在一天後就更新好了。

---

* ### Guideline 1.5 - Safety - Developer Information
> The support URL specified in your app’s metadata, < your webpage >, does not properly navigate to the intended destination.
* #### 信件大綱
  在 App 的介紹資訊中，沒有提供足夠的開發者資訊。
* #### 導致原因
  由於這是 Side Project，所以在 App 的介紹頁面中，提供支援的網頁內容，無提供聯絡方式導致。
* #### 解決方式
  提供支援的網頁內容，加上聯絡方式即可。我是加上電子信箱後再次送審，即沒有被退件。

---

* ### Guideline 2.1 - Information Needed
> This type of app has been identified as one that may violate one or more of the following App Store Review Guidelines. Specifically, these types of apps often:
> 
> 1.1.6 - Include false information, features, or misleading metadata.
> 
> 2.3.0 - Undergo significant concept changes after approval
> 
> 2.3.1 - Have hidden or undocumented features, including hidden “switches” that redirect to a gambling or lottery website
> 
> 3.1.1 - Use payment mechanisms other than in-app purchase to unlock features or functionality in the app
> 
> 4.3.0 - Are a duplicate of another app or are conspicuously similar to another app
> 
> 5.2.1 - Were not submitted by the legal entity that owns and is responsible for offering any services provided by the app
> 
> 5.3.4 - Do not have the necessary licensing and permissions for all the locations where the app is used
> 
> Before we can continue with our review, please confirm that this app does not violate any of the above guidelines. You may reply to this message in Resolution Center or the App Review Information section in iTunes Connect to verify this app’s compliance.
> 
> Given the tendency for apps of this type to violate the aforementioned guidelines, this review will take additional time. If at any time we discover that this app is in violation of these guidelines, the app will be rejected and removed from the App Store, and it may result in the termination of your Apple Developer Program account.
> 
> Should you choose to resubmit this app without confirming this app’s compliance, the next submission of this app will still require a longer review time. Additionally, this app will not be eligible for an expedited review until we have received your confirmation.
* #### 信件大綱
  App 被判定為違反這七條其中一個條例，或者違反更多條例，請確認沒有違反這些條例後，再回覆 Apple，審核才會繼續。如果之後發現你違反了其中一條，App 可能會從 Apple Store 上被移除，開發者帳號可能被封鎖。
* #### 導致原因
  這個訊息列出七條，但是這個 App 已經上架約五年，照理來說已經不會違反這幾個條例。Google 後發現其實滿多人有收到這個訊息的，而且都是今年(2018年初)送審時收到的，訊息完全一模一樣，一字不差。提供的訊息也很籠統，所以大家是認為這是(machine learning)機器審核，所以才會這樣判斷。
* #### 解決方式
  說明你的 App 並沒有違反他列出的每一條條例即可。另外他的條列其實也涵蓋了很大的範圍，比如2.3.1，認為你有可能會導到博奕網站。但只要有用 webView 都有可能會導過去。比如5.3.4，沒有使用 location 的必要。但我沒用到 location，也沒有跟使用者要權限。所以這裡還是有很大的空間可以讓你向 Apple 解釋。最後複審時，等了快一個月才過。
---

* ### Guideline 2.1 - Performance - App Completeness
  
> We discovered one or more bugs in your app when reviewed on iPhone and iPad running iOS 11.2.1 on Wi-Fi connected to an IPv6 network.
> 
> Specifically, we found that your app does not load any content under the “A頁面“ section. Please see attached screenshots for details.
> 
> Next Steps
> 
> To resolve this issue, please run your app on a device to identify any issues, then revise and resubmit your app for review.
> 
> If we misunderstood the intended behavior of your app, please reply to this message in Resolution Center to provide information on how these features were intended to work.
> 
> For new apps, uninstall all previous versions of your app from a device, then install and follow the steps to reproduce the issue. For updates, install the new version as an update to the previous version, then follow the steps to reproduce the issue.
* #### 信件大綱
  我的某個頁面沒有載入任何資料。請我確認。並詢問是否誤解我的 App 行為。
* #### 導致原因
  當我開發這個 App 時，此頁面的資料來源是正常且格式已經確定。上架前，後端臨時需要更改，所以先關閉 API。而 App 在此頁由於有做檢查資料的機制，所以不會閃退，但是會無法從 API 撈到資料。
* #### 解決方式
  先把此頁面入口隱藏。
  回信跟 Apple 說明。當後端資料完整後，使用者不需要更新 App，那一頁就會直接顯示。但也有可能被認為你的 App 這樣尚未完成，依然駁回。(我是得到這一個結果)

---

* ### 4.0 Before You Submit: Info Needed (iOS)(Invalid sdk value)
> Dear developer,
> 
> Information Needed
> 
> We began the review of your app but aren’t able to continue because we need additional information about your app.
> 
> At your earliest opportunity, please review the following question(s) and provide as much detailed information as you can. The more information you can provide upfront, the sooner we can complete your review.
> 
> What is the name of the retailer of your app?
> 
> If your app provide a platform to multiple retailers, what is the name of the company that owns this app/platform?
> 
> Once you reply to this message in Resolution Center with the requested information, we can proceed with your review.
* #### 信件大綱
  需要回答兩個問題。 
  1. What is the name of the retailer of your app? 
  2. If your app provide a platform to multiple retailers, what is the name of the company that owns this app/platform?
* #### 導致原因
  這問題就是 Apple 詢問你的 App 資訊/公司資訊，通常只有在你 App 剛上架的前幾次會遇到。但是我的 App 已經上架一年多的時間了，還收到這訊息，所以只好再說明一次。
* #### 解決方式
  回答他想知道的資訊，說明你的 App 資訊/公司資訊即可。如果你的 App 有登入的功能，建議提供一組測試用的帳密給 Apple 測試。

---

* ### Guideline 4.2.2 - Design - Minimum Functionality
> We noticed that your app only includes links, images, or content aggregated from the Internet with limited or no native iOS functionality. We understand that this content may be curated from the web specifically for your users, but since it does not sufficiently differ from a mobile web browsing experience, it is not appropriate for the App Store.
> 
> The next submission of this app may require a longer review time, and this app will not be eligible for an expedited review until this issue is resolved.
* #### 信件大綱
  App 的內容與網頁並沒有太大差異，且也沒有因為做成 App 就與網頁有不一樣的體驗。所以被認為不適合上架至 App Store。
* #### 導致原因
  這個 App 其實只是把後台爬蟲到資料庫的內容顯示出來。已經有網頁版本。但是在 Apple 審核時認為 App 與 Web 使用上並沒有什麼差異，所以不願意讓 App 上架。其實在這支 App，有一個加到我的收藏的功能，Web 是沒有的。
* #### 解決方式
  說明你的 App 與 Web 的差異給 Apple 審核人員了解。但是要注意，不能因為這個差異，反而讓 App 在這個差異下，使用者無法使用。例如：只有 App 才有會員功能，而沒有登入會員，App 就無法走下去。

---

* ### Guideline 2.3.10 - Performance - Accurate Metadata
> We noticed that your app or its metadata includes irrelevant third-party platform information.

> Specifically, your app includes non-iOS platform references in the What’s New text.

> Referencing third-party platforms in your app or its metadata is not permitted on the App Store unless there is specific interactive functionality.
* #### 信件大綱
  在 App 資訊中，包含第三方的資訊。準確的說，是有包含非 iOS 平台的文字。除非有特定用途，否則不允許在 App Store 上出現第三方資訊
* #### 導致原因
  因為在 App 更新功能描述中出現了「Google」的文案導致。之前也有遇過預覽圖中，有搜尋教學，但是因為搜尋的是 Android 的手機，所以被退件的案例...
* #### 解決方式
  寫文案時，不使用第三方的關鍵字。或是不寫相關文案即可。

---

### Guideline 5.1.2 - Legal - Privacy - Data Use and Sharing
> The app privacy information you provided in App Store Connect indicates you collect data in order to track the user, including Device ID. However, you do not use App Tracking Transparency to request the user's permission before tracking their activity.

> Starting with iOS 14.5, apps on the App Store need to receive the user’s permission through the AppTrackingTransparency framework before collecting data used to track them. This requirement protects the privacy of App Store users.
* #### 信件大綱
  在 App 的隱私權中，有提示會使用追蹤資料，但是在 App 內沒有使用。且從 iOS 14.5 開始，有使用追蹤資料，都要使用 AppTrackingTransparency，必須保護使用者安全。
* #### 導致原因
  因為原本因應 Google Admob，所以有使用追蹤，但後來認為不是必要，故又移除，但在 App 的隱私權中並未移除。
* #### 解決方式 
  在 App 的隱私權中移除，讓他與 App 要求的權限一致。

<!-- * 
### Invalid sdk value
* #### 信件大綱
* #### 導致原因
* #### 解決方式 
* -->
