---
title: "如何讓 iOS App 在 release 版本時不會印出內容"
author: "Ken Chang"
tags: ["iOS", "MacOS", "Swift", "Objective-C"]
date: 2020-06-01T00:00:00+08:00
comments: true
draft: false
toc: true
---

在寫程式時，常會把一些資料印出來檢查，但是在 archive 後的 release 版裡，當執行到印出的程式碼，實際上裝置還是有印出，只是使用者沒有介面可以看得。這時就等於多花效能去執行對使用者沒有用處的程式碼。甚至有時輸出的數據還有可能會造成資安問題。

較好的方式是在 archive 正式版的時候，把輸出程式碼都註釋或移除，但是當然不會每次 archive，真的就掃過所有的輸出程式碼。比較好的做法是透過 flag，辨認正在編譯的版本是 release 或 debug ，決定需不需要把印出程式碼編譯進去。

<!--more-->

---

## Swift 作法
- 在 release 的版本，不需要 print 的寫法。做好後在程式碼裡就都使用 dPrint 取代 print 。
{{< gist KennC e3b577f5ccc0b9cac09103eb1bbbb2e1 >}}

- 也可以透過一些符號，自訂想要的訊息。（ 可參考喵神的文章：[LOG輸出](http://swifter.tips/log/) ）
{{< gist KennC ef9a976bf02660b7d1149f2d20e3eb0f >}}

---

## Objective-C作法
要讓程式在 runtime 時使用我們的格式，要寫在 [appName]-Prefix.pch 裡。判斷用的參數 DEBUG ，在專案的 Build Settings -> Apple LLVM compiler 7.1 底下 Preprocessing。預設為true。
注意：在 Xcode6 之後，預設不會建立 pch ，需要自行建立，建立好以後要重新編譯。

- 在 release 的版本，不需要 NSLog() 的寫法。這個方式的原理是因為 release 模式會定義__OPTIMIZE__，而 debug 模式不會。
{{< gist KennC 2298bc717eb66d0a5bd9a693fe3b1bf1 >}}

- 比較有彈性的方式是這樣寫。在 DEBUG 模式時使用 DLog ， DLog 功能在這裡就是與 NSLog 相同(下方有自定義 DLog 的方式)。非在 DEBUG 模式時， DLog 看成註解。 ALog 則是不論在 DEBUG 模式或是為 release 模式都會輸出。
{{< gist KennC 94f19ca9bedca8115cc435bea9c0ba3f >}}

- 如果有想要更詳細的 DLog ，與 Swift 相同，可以自訂想要的訊息。
{{< gist KennC 479b43facc6cab1149613dd64d0c005d >}}
