---
title: "開發 iOS/MacOS 的常用工具及套件"
author: "Ken Chang"
tags: ["Tools","iOS","MacOS"]
date: 2020-06-01T00:00:00+08:00
comments: true
draft: false
toc: true
---

介紹平常開發 iOS/MacOS 常用的軟體開發工具及各種套件，這些一直在推新，有時因應不同需求，就要再去找不同工具使用。記錄這篇主要因為最近使用新電腦，需要安裝許多工具，覺得下次還會用到。紀錄這些工具在我的開發上大致是什麼作用，許多工具也都是看別人分享，使用後覺得順手好用，就會留下來。這些工具大部分都是免費使用，或是試用幾個月後，轉正版只要幾千塊，如果覺得好用可以贊助支持，讓公司可以持續保持軟體的健康。

<!--more-->

---

## XCode

* 連結：[http://developer.apple.com/xcode/](http://developer.apple.com/xcode/)
* 簡介：要開發 iOS/MacOS 必備的 IDE。既然是 IDE，當然可安裝外掛，強化它的功能。這幾年官方都把好用的外掛功能，整合進 XCode，所以我已經很少在裝外掛了。只會改 Theme 成 WWDC17-Xcode-Theme。XCode會隨著你的使用，裡面的資料夾容量會越來越大，所以每過一陣子要自己去清理裡面某幾個資料夾。也可以從官方開發者網站下載。

---

## WWDC17-Xcode-Theme

* 連結：[https://github.com/mozharovsky/WWDC17-Xcode-Theme](https://github.com/mozharovsky/WWDC17-Xcode-Theme)
* 簡介：Xcode的Theme，當然網路上還有很多不一樣的，但我覺得這個配色最柔和。

---

## CocoaPods

* 連結：[https://cocoapods.org](https://cocoapods.org)
* 簡介：方便管理第三方套件的工具，在環境裡安裝好後，project 新增一個 Podfile，從此以後要安裝、更新、刪除第三方套件，都只要輸入幾個指令就可以了，很多第三方套件也都有支援使用 CocoaPods 安裝。如果有使用 CocoaPods 管理專案，都是要從 .xcworkspace 開啟。同類型的還有 Carthage 跟官方的 Swift Package Manager(SPM)。現在 Xcode 已經內建 SPM，覺得會逐漸取代掉主流 CocoaPods。

---

## Alcatraz

* 連結：[http://alcatraz.io](http://alcatraz.io)
* 簡介：Xcode裡幫你管理plugins的工具，基本上裝完Xcode後，會直接安裝上去。接著用Alcatraz安裝慣用的 theme、highlight、圖名補齊...各種自己用習慣的plugins，這還可以拿來安裝 CocoaPods。個人是會裝 Backlight、KSImageNamed，再選一個當下看起來順眼的 theme。這邊要注意的是有些 plugins 安裝後，重新啟動 Xcode，bundle 後要去找啟用路徑啟用，路徑一般在作者的github會有寫。

---

## Git

* 連結：[https://git-scm.com](https://git-scm.com)
* 簡介：版本控制必備，是各位工程師一定會有的東西。管理 repo 的 commit、push、pull...etc，還有 branch、reset、stagh...etc，各種版本或檔案的記錄、回朔、暫存...等。git功能非常強大，自己沒有架設 git 的話，能使用 [bitbucket](https://bitbucket.org) 或是 [Github](https://github.com) 即可使用到強大的管理功能。多人開發可以合併...。初學者推薦網路上有個很棒的教學 [連猴子都能懂的Git入門指南| 貝格樂](https://backlogtool.com/git-guide/tw/)

---

## SourceTree

* 連結：[https://www.sourcetreeapp.com](https://www.sourcetreeapp.com)
* 簡介：可以控制 git 的 GUI 介面工具，不太會下 command 的人可以先用 GUI 工具，習慣 git 這個概念，之後有機會學習改用 command，可以控制更細節的步驟。這種 GUI 工具有不少，像 github 也有出 GitHub Desktop。

---

## Faric(已被整合進Firebase)

* 連結：[https://get.fabric.io](https://get.fabric.io)
* 簡介：Twitter 收購的工具，可以追蹤崩潰訊息、分析崩潰訊息、也可以發版本測試用的工具，簡單好用。官網的教學也寫得非常清楚。需要的功能跟著步驟做，做對做錯都會有 responce，建議也用 CocoaPods 安裝就好。

---

## Paw

* 連結：[https://paw.cloud](https://paw.cloud)
* 簡介：好用的 API 測試工具，可以做 oauth，內建許多常見的加密演算法，是我試用過後第一個購買的軟體。如果不想花費，可以用 Postman。

---

## Postman

* 連結：[https://www.getpostman.com](https://www.getpostman.com)
* 簡介：跟 Paw 類似的 API 測試工具，其實同事大部分是用這個，因為免費，功能也足夠。

---

## Charles

* 連結：[https://www.charlesproxy.com](https://www.charlesproxy.com)
* 簡介：Mac 好用的抓包軟體，也可透過它來做點有趣的檢查。例如：中間人攻擊。

---

## iTerm

* 連結：[https://www.iterm2.com](https://www.iterm2.com)
* 簡介：其實就是加強版終端機。

---

## Oh My Zsh

* 連結：[http://ohmyz.sh](http://ohmyz.sh)
* 簡介：zsh framework。

---

## Homebrew

* 連結：[https://brew.sh/index_zh-tw.html](https://brew.sh/index_zh-tw.html)
* 簡介：用 ruby 做的套件管理工具，用 Mac 的開發者必裝。

---

## Swimat

* 連結：[https://github.com/Jintin/Swimat](https://github.com/Jintin/Swimat)
* 簡介：swift語法的排版工具。
