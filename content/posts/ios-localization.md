---
title: "iOS 多國語系 (在App內部切換語系)"
author: "Ken Chang"
tags: ["iOS", "MacOS", "Swift", "Objective-C"]
date: 2020-06-03T00:00:00+08:00
comments: true
draft: false
toc: true
---

<!--more-->
<!------->
<!--## TL;DR-->
<!--* ##### 需求-->
<!--  * ##### 根據「系統的語系」-->
<!--  * ##### 根據「使用者在 App 裡選擇的語系」-->
<!--  * ##### 調整需求-->
<!--* ##### 常見問題-->
<!--    * ##### App name、icon 與 LaunchScreen 可以做嗎？-->
<!--    * ##### 圖片根據語系而有不同的話，可以做嗎？-->
<!--    * ##### 如果是使用 Storyboard 開發，Storyboard 元件上的文字，可以做嗎？-->
<!--* ##### 流程-->
<!--  * ##### 根據「系統的語系」流程-->
<!--  * ##### 根據「使用者在 App 裡選擇的語系」流程-->
<!--* ##### 實作-->
<!--  * ##### 文字-->
<!--  * ##### 圖片(根據系統的語系)-->
<!--  * ##### App Name 與 LaunchScreen(根據系統的語系)-->
<!---->
<!------->
## 需求
當接到這多國語系需求的時候，首先我們需要確認一件事。需求是 App 的語系要根據「系統的語系」還是「使用者在 App 裡選擇的語系」？兩者可說是完全不同的功能。所以當接到需求是要做多國語系時，工程師需要問清楚。因為需求單位常常也不知道這兩種是不同的。如果要根據「使用者在 App 裡選擇的語系」，還會需要切換語系的入口跟選擇語系的畫面。
- #### 根據「系統的語系」

    `使用者無法自行在 App 內部切換語系，App 語系跟著系統的語系。`
    系統是什麼語系，App 就是什麼語系。舉例來說，你的系統語系是英文，App 就顯示英文，如果系統的語系在 App 裡沒有支援，App 的語系會是 App 的預設語系(Base)。

- #### 根據「使用者在 App 裡選擇的語系」

    `使用者可自行在 App 內部切換語系，App 內部新增切換語系功能。`不論系統語系是什麼，App 就是使用者選的語系。舉例來說，你的系統語系是英文，但是使用者在 App 裡選擇中文，App 就是顯示中文。不論系統語系如何調整，App 語系都會是中文。</br>

- #### 調整需求

    在需求方了解以後，通常會調整需求成這樣：
    * 第一次開啟App時，根據系統的語系決定App語系。
    * 使用者選擇過語系後，App 語系就會是使用者選擇的語系。

如果確認需求只要根據「系統的語系」，那麼第一點做完就完成需求了。如果需求要可在 App 內部切換語系，就會思考在使用者未曾選過語系時，要使用什麼語系，此時也只有系統語系可以選。至於要不要把第一次開啟App的語系記錄下來，在使用者選過語系以前，都使用第一次開啟的語系。或是不紀錄，每一次開啟都根據系統的語系決定App語系，就要看功能如何規劃。

---
## 常見問題

* #### App name 、icon 與 LaunchScreen 也可以做嗎？
    App name 與 LaunchScreen 我測試是可以做根據「系統的語系」，但是無法做根據「使用者在 App 裡選擇的語系」。icon 原無支援，但在 iOS 10.3 後，可在使用者開啟 App 時，更新預先放在 App 裡的 icon，可達到效果。但通常 icon 不會跟著做。

* ##### 圖片也會根據語系不同的話，也可以做嗎？
    ~~圖片只支援根據「系統的語系」。且要支援的圖片無法使用 Assets.xcassets 管理。要拉出來以後，放在.lproj語系包裡才可以做到。~~ 近期做了某些功能有研究 Assets.xcassets，覺得有機會可以。

* ##### 如果是使用 Storyboard 開發，Storyboard 元件上的 text，可以做嗎？
    在 Storyboard 上的元件，只有支援根據系統語系而改變。如果要做成在 App 裡選擇的語系，就需要拉出 IBOutlet，使用程式碼的方式指定文字。

---
## 流程
* #### 根據「系統的語系」流程
  1. 啟動 App，讀取現在系統語系。
  2. 根據系統語系選擇語系包，若無對應語系包，則選擇預設語系(Base)。

* #### 根據「使用者在 App 裡選擇的語系」流程
  1. 啟動 App，從 UserDefaults 取得使用者曾選過的語系，設置 bundle 的語系包。
  2. 如果沒有曾選過的語系，就使用系統語系或是預設語系。
  3. 使用者觸發切換語系。切換 bundle 的語系包，將 bundle 的語系紀錄在 UserDefaults。
  4. 刷新畫面。

---
* ## 實作
  * #### 文字

    不論是想要做哪一種都要先有語系的語系包，才能指定。擁有語系包，再寫個管理語系的 manager，把相關程式整理進去即可。

    * ##### Step 1. 新增語系
      到 PROJECT > Info頁籤 > 點擊 "+"
      ![](https://images2.imgbox.com/2c/dd/ktVtmmKL_o.png)

    * ##### Step 2. 新增文字檔
      資料夾右鍵 > New File... > 選擇 Strings File > 檔案名稱一定要命名為 `Localizable`
      ![](https://images2.imgbox.com/6c/71/DOKaHEai_o.png)

    * ##### Step 3. 點擊剛創的Strings File右方的 Localize...
      ![](https://images2.imgbox.com/9f/68/08hJUELL_o.png)

    * ##### Step 4. 勾選需要的語系
      ![](https://images2.imgbox.com/19/9f/6HI3vcJr_o.png)

    * ##### Step 5. 設定好你要顯示在不同語系的字串
      格式固定是`Key = String ;`實作上這部分會是最花時間的，因為要把專案裡所有的文字都取一個Key抽出來，當初如果有做現在就輕鬆了。(Android 則是專案開始就有獨立的文字檔案)。
      Key 沒有固定格式，一般會使用畫面做分類，用底線拆開單字，全大寫或全小寫。這裡 Key 的命名有點小技巧，建議以畫面+元件+功能，比如：`loginVC_login_button_login = "登入";`
      因為翻譯的人不見得知道這個字串顯示在畫面上的哪邊，而無法決定翻譯或是大小寫。所以 Key 這樣的命名可以幫助理解。

    * ##### Step 6. 實作用來管理語系的class
      語系資源準備好後，做個管理語系的 class 在專案裡，管理與設定語系相關的程式。我在使用 Objective-C 和 Swift 在取字串時，有不同的作法。
      Objective-C 用 define，Swift 用 String Extension 。
      一般 App 只會有一個文字檔，但在我的 Objective-C 範例裡有兩個 define，使用帶 Key 的 define 就可以了。需要特別指定哪個 table 時再使用帶 table 的 define。Swift 範例裡我就直接用 String Extension 取字串。

    * ##### LanguageTool.h
      {{< gist KennC 880b8af7b40e50d38a5a2ffd3f698a89 >}}

    * ##### LanguageTool.m
      {{< gist KennC c4da4be1745127f576bb26ae642109a9 >}}

    * ##### 元件使用 define 設定字串
      {{< gist KennC 4b5dc4f60fde9a430168d8cf045281eb >}}

    * ##### 切換時呼叫方法
      {{< gist KennC ffb591a5d4e5b23d4a3b68ec65a808a6 >}}

  * #### 圖片(系統的語系)

    將圖片放在對的語系包，圖片使用相同的名字，系統就會自動轉換。

    * ##### 將圖片拉近專案，點擊右方的 Localize後，在不同的語系包裡使用同名但不同圖片的檔案。
      ![](https://images2.imgbox.com/3e/8f/87tL1vMr_o.png)

      即使在不同語系包，但因圖片名稱是一樣的。所以並不用透過Key再取圖片名稱。
      {{< gist KennC 008c63e74bd71d54ec74872c9b3dabb6 >}}

  * #### App Name 與 LaunchScreen(系統的語系)

    * ##### 新建一個LaunchScreen
      資料夾右鍵 > New File... > 選擇 LaunchScreen > 自訂名字

    * ##### 指定不同語系的 App Name 與 LaunchScreen
      資料夾右鍵 > New File... > 選擇 Strings File > 檔案名稱一定要命名為命名 `InfoPlist`。
      `"CFBundleDisplayName"`這是 App Name 的 key。
      `"UILaunchStoryboardName"`這是 LaunchScreen 的 key。
      ![](https://images2.imgbox.com/a6/8e/dFbTsSuc_o.png)

---

以上的 Objective-C 範例原始碼，放在[Github連結](https://github.com/KennC/ObjC_Localization)提供參考。

Swift 版本待續...
