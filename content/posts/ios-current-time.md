---
title: "計算 iOS 程式碼執行花費時間"
author: "Ken Chang"
tags: ["iOS", "MacOS", "Swift", "Objective-C"]
date: 2020-06-01T00:00:00+08:00
comments: true
draft: false
toc: true
---

有時需要確認一段程式碼從"開始執行到執行完畢"花多久時間。程式碼在本機上執行的其實都很快，即使沒有做優化，都使用一般的寫法，也是多花一些記憶體，或是多個毫秒，要使用者有感體驗，通常要大規模的優化。
那何時會需要呢？比如有連結網路，或是外部裝置時，用以確認成功連結需要花費多少時間。或是有時間複雜度的問題需要確認。也有人用來查看元件生成的速度，有些原生元件其實用 Layer 層來做會快很多。

這邊用 CFAbsoluteTimeGetCurrent 跟 NSDate 有微秒的準度，如果你有奈秒的需求，可以找 CACurrentMediaTime 或是 dispatch_benchmark (Privacy API)。

<!--more-->

* ## 使用 CFAbsoluteTimeGetCurrent

{{< gist KennC f8636358b5a05432a5322169bc7ca935 >}}

如果要連續計算好幾段程式碼，會需要在多幾個以下這部分。可依照需求，重複穿插在程式碼裡。
{{< gist KennC d2a13652c2dc6eec814afcde4e2fbf00 >}}

---

* ## 使用 NSDate

Objective-C
{{< gist KennC 92a2f7165d03cb2381e9c5eb7be821da >}}

Swift
{{< gist KennC 0511d7d420b8071c6e274463fd62759b >}}
