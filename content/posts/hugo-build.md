---
title: "如何建置 Hugo 並部署在 Github Pages"
author: "Ken Chang"
tags: ["Hugo", "Blog", "Git", "GitHub"]
date: 2020-06-01T00:00:00+08:00
comments: true
draft: false
toc: true
---

現在很多靜態部落格框架，對工程師來說都很容易架起來，也可以利用一些免費資源，建置在雲端上。舉例來說 Jekyll、Hexo、Hugo 建置在 Github Pages、Gitlab Pages、Bitbucket Pages，都是完全免費。建置過程會遇到一些小問題，但 Google 一下都有解。這篇文章就是紀錄在 Github Pages 建置 Hugo 過程，以及遇到的疑難雜症。

<!--more-->

---

## 安裝Hugo

由於我是使用 MacOS，所以只介紹 MacOS 的建置過程。首先我們電腦裡要安裝 Hugo，官方也有提供[使用Homebrew安裝](https://gohugo.io/getting-started/quick-start/)，直接下個指令即可。這就是 [Homebrew](https://brew.sh/index_zh-tw) 好用的地方。

>brew install hugo

接著到你要放置部落格的資料夾下指令

> hugo new site `<name>`

你就會看到訊息：

> Congratulations! Your new Hugo site is created in `<path>`

你成功產生了一個靜態的部落格。

我們來看看這個資料夾裡有什麼，稍微了解用途，對修改時會有幫助。

├─ archetypes/

├─ config.toml

├─ content/

├─ data/

├─ layouts/

├─ static/

└─ themes/

archetypes：就是你的 front matter，在你 new 一篇文章時，會自動幫你加入的部分。

config.toml：設定檔，可調整的參數都會在這支檔案裡，這裡會因應你要的功能，來決定要調整哪些參數。

content：放文章的資料夾，官方建議在底下開子資料夾做管理。

data：存放取得資料語法的資料夾。可以看[官方說明](https://gohugo.io/templates/data-templates/)。

layouts：顯示網頁的 html，但因為有[look up order](https://gohugo.io/templates/homepage/#homepage-template-lookup-order)，實際的 html 都會在 themes 底下的 layouts，這樣換主題時也較方便。

static：存放一些檔案或是圖片的資料夾。

themes：存放主題的資料夾。

---

## 修改主題

接著我們 cd 到 theme，把主題 clone 下來，目前我使用的主題是 hugo-nuo

> git clone https://github.com/laozhu/hugo-nuo


這時，有著你自己選的主題的部落格就可以透過在根目錄下，輸入指令看到了

> hugo server

輸入指令後，你就可以在 http://localhost:1313/ 看到，這邊還只是在本機，一個部落格這樣就做好了。

如果沒有看到，請到 config.toml 加上 theme = "\< ur theme name\>"

---

## 上傳到Github

接著回到根目錄，使用 hugo 這個指令，就會產生 public 資料夾，hugo 已將靜態頁面產生在裡面。這個資料夾內容就是我們要推上去 Github 的網頁內容。

>hugo

到你的Github開一個repo名稱就叫`<github username>.github.io`(這名字是固定的)，取得他的 repo url，接著把你的 public push 上去。

cd 到 public 底下做好初始化 git，就可以直接 push 了

> cd public 
>
> git init git remote add origin `https://github.com/<github username>/<github username>.github.io.git`(剛剛的repo url) 
>
> git add .
>
> git commit -m "first commit" 
>
> git push -u origin master

如果在 github 看到有推上去，以我的經驗。馬上就可在 `https://<github username>.github.io/` 看到你的網站了！


---
