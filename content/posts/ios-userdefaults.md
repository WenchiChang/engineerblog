---
title: "關於 NSUserDefaults 以及五個 Domain"
author: "Ken Chang"
tags: ["iOS", "MacOS", "Swift", "Objective-C"]
date: 2020-06-01T00:00:00+08:00
comments: true
draft: false
toc: true
---
NSUserDefaults 是 iOS 用來儲存資料的類別。跟 NSDictionary 相同，使用字串當 key 存取數據，並以 .plist 格式存在 App 裡。主要保存用戶的 App 設置和一些較為簡單的數據，例如：是否第一次登入、是否自動登入、版本資訊、語言設定...。通常大家只要會新增跟取用，就可以很方便的在 App 每個地方存取數據。先檢視一下官網的說明，可以儲存哪些資料。

<!--more-->

<!--## TL;DR-->
<!--### Apple 官網說明可以儲存的類型-->
<!--* ##### 如何看到這份儲存好的.plist-->
<!--* ##### NSUserDefaults在使用容易-->
<!--    * #### 儲存-->
<!--    * #### 讀取-->
<!--    * #### 清除-->
<!--* ##### 為何要馬上synchronize-->
<!--* ##### 一次移除NSUserDefaults裡的全部資料-->
<!--    * ##### 逐筆刪除-->
<!--    * ##### 移除持久域-->
<!--* ##### 不使用NSUserDefaults儲存複雜或大量的資料-->
<!--* ##### 私密資訊請使用 Keychain 儲存 -->
<!--* ##### 關於 NSUserDefaults 的五個 Domain-->
<!--    * ##### NSArgumentDomain-->
<!--    * ##### Application-->
<!--    * ##### NSGlobalDomain-->
<!--    * ##### Languages-->
<!--    * ##### NSRegistrationDomain-->

---

## Apple 官網說明可以支援儲存的類型
  
- NSData
- NSString
- NSNumber
- NSDate
- NSArray
- NSDictionary
    
為什麼是這六種類型？因為 NSUserDefaults 儲存在 project 裡時，是以 .plist 的格式存在。如同你在更改 .plist 時，他只出現這幾種支援格式給你選擇。

---

## 如何看到這份儲存好的.plist

進入中斷點時，在 Xcode 的 terminal 輸入 po NSHomeDirectory()，會得到一個<path>，再到終端機裡`open <path>` ，進入 Library > Preferences，就可以在裡面看到那份 plist。

![](https://images2.imgbox.com/c7/38/4tTV66y5_o.png)

---

## NSUserDefaults在使用儲存、讀取、移除上非常容易

* ##### 儲存：
{{< gist KennC e808134f90ece279f02894cf1c5d1921 >}}

* ##### 讀取：
{{< gist KennC 737eb1f81efa6b524ae04b3ed842daed >}}

* ##### 移除：
{{< gist KennC 69be54f93a93547f24946ae3e323ba19 >}}

---

## 為何要馬上synchronize

在把資料 set 進 userDefaults 時，其實系統並不會馬上儲存，系統會等到有資源時才同步進手機，所以立即 synchronize，避免產生還沒儲存前資料就遺失的問題。其實這狀況在早期的開發比較容易發生，現在裝置的處理速度已很少遇到這問題，但為了避免發生，還是都會在設定好資料後，立即同步。

---

## 一次移除NSUserDefaults裡的全部資料

有些情境會需要清掉 NSUserDefaults 的資料，例如：登出使用者帳號。為了避免同一裝置的第二位使用者登入帳號時，存取到上一位使用者的設定。或是某些數值以 NSUserDefaults 裡的資料判斷的開關，而需要回復至預設狀態。方法有兩種，第一種是取得 dictionary 後，逐筆刪除。第二種方法是直接移除持久域。較推薦第二種方法。

* ##### 逐筆刪除
    {{< gist KennC ea65d283969b031d21f4a879f8154243 >}}

* ##### 移除持久域
    {{< gist KennC 0648839f6d1fbdb3bcca69ef480753eb >}}

---

## 不使用NSUserDefaults儲存複雜或大量的資料

複雜大量的資料就是使用資料庫儲存，而不是使用 NSUserDefaults 了。例如：看過的商品紀錄、GPS記錄...。iOS的儲存通常使用以下這幾種方式：
- 使用 Core Data。
- 使用 SQLite。或是 base 於 SQLite 的 third party，例如：FMDB。
- 使用 Realm，或是其他資料庫套件。需要注意安裝後打包 App size，看值不值得。
- NSCoding 序列化以後存成 file。

這幾種如何挑選適合的使用?就要看需求。我的選擇方式通常如下：如果是一次就需要取得全部數值，顯示給使用者看，就使用序列化儲存。例如：瀏覽紀錄。如果是要從一堆資料中取一個或數個數值，就使用資料庫。序列化儲存可以參考這篇文章[自定義類別歸檔儲存(序列化)](http://www.kcbox.io/serialization/)。

這兩種有什麼不同?我們使用序列化儲存時，會存成一個檔案，而開啟檔案時，是把這份檔案放一份到memory。而使用資料庫，是在做完query以後，將query的數值放到memory，不會把整個資料庫放到memory。所以如果我們不需要開啟整份資料，就使用資料庫做query，做完後只取結果即可。

---

## 私密資訊請使用 Keychain 儲存 

有些 App 會將使用者的私密資料(例如：信用卡資料、帳號、密碼...)存在 NSUserDefaults，然而還有更適合的做法是使用 Keychain 儲存，為何會這麼說呢?因為NSUserDefaults 是以明碼的方式存在 Sandbox的.plist，而 Keychain 是儲存加密過的資料(例如：密碼、序列號、證書...)，會將數據加密後再儲存，不在 Sandbox 裡。
Keychain 的缺點就是當 App 被刪除時，資料依然會存在裝置的Keychain裡，不會被移除。另外如果裝置有越獄，無論是 Sandbox的.plist 或是 Keychain，都是會可以被導出來的。

---
## 關於 NSUserDefaults 的五個 Domain

NSUserDefaults 初始化時，會默認五個 Domain，加上這種設計後，就不單純只有儲存信息的功能。
搜尋優先順序是
NSArgumentDomain > Application > NSGlobalDomain > Languages > NSRegistrationDomain

- NSArgumentDomain
    - NSArgumentDomain 是最先會被搜尋到的啟動參數，可以在 Arguments Passed On Launch 加入。
    例如加入一個 key 是 aStr，value 是 MyValue。
    就可以在程式裡以 [userDefaults objectForKey:@"aStr"] 取得 MyValue。

- Application
    - 這算是最常用到的，你在 App 裡使用 `set:forKey:` 的都是儲存在這個域。

- NSGlobalDomain
    - 系統級別的全局域，可儲存系統的配置，也可以用來再 App 之間傳值。

- Languages
    - 這個是語言的設置，其實 NSLocalizedString 就是使用 NSUserDefaults 的 Languages 來設定當前的語言環境。他的 key 就是 AppleLanguages。

- NSRegistrationDomain
    - 這個就是最後會被搜尋到的 Domain，要是有在使用 registerDefaults，就是將資料註冊到這個域。這個域不會保留資料，所以每次開啟 App 都要重新註冊。
