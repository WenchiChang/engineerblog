---
title: "自定義類別歸檔儲存(使用 NSCoding 序列化) "
author: "Ken Chang"
tags: ["iOS", "MacOS", "Swift", "Objective-C"]
date: 2020-06-01T00:00:00+08:00
comments: true
draft: false
---

要儲存的自定義類別需要遵守 NSCoding 這個 protocol，並實作以下這兩個 method。

<!--more-->

{{< gist KennC 9795187fb4f4994c1cc7d7c8300a8e72 >}}

#### 實作

我們自定義一個 ItemDataModel 類別，裡面有 NSString、NSDate、NSInteger，並將 ItemDataModel 序列化儲存。

{{< gist KennC a000f0118067f1a4a24d4c3ceb7212b9 >}}

{{< gist KennC 6d24d059b9eb46a40b8948b14cd9eef2 >}}

以上就是一個有實作 NSCoding 的自定義類別 ItemDataModel。

#### 如何使用

初始化一個 ItemDataModel，設定好資料後儲存。由於已經有實作序列化，所以可以順利儲存自己定義的
{{< gist KennC f441ee0760163c83eb6691262b94b58e >}}

取得資料
{{< gist KennC 31e17b4c08a850b7d13efc38b8dd446d >}}
