+++
title = "About Me"
description = "Hugo, the world's fastest framework for building websites"
date = "2020-01-19"
aliases = ["about-us", "about-hugo", "contact"]
author = "Ken Chang"
+++

Hello! I'm Ken Chang. An iOS mobile engineer since 2013. 

Developed apps about e-commerce, community, compare prices, rebate, hail a cab.

Here is a record of my development experience and related ideas. Hope you also find what you want here.
